USE [SRTSApp]
GO
/****** Object:  Table [dbo].[BillMaster]    Script Date: 6/28/2020 5:07:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillMaster](
	[BillId] [int] IDENTITY(1,1) NOT NULL,
	[BillNo] [varchar](50) NULL,
	[BillDate] [date] NULL,
	[ClientId] [int] NULL,
	[FromStation] [int] NULL,
	[ToStation] [int] NULL,
	[WeightId] [int] NULL,
	[Freight] [decimal](18, 2) NULL,
	[Loading] [decimal](18, 2) NULL,
	[UnLoading] [decimal](18, 2) NULL,
	[DetentionCharge] [decimal](18, 2) NULL,
	[TotalAmt] [decimal](18, 2) NULL,
	[TotalAmtWords] [varchar](100) NULL,
	[Remarks] [varchar](200) NULL,
	[OwnerId] [int] NULL,
	[CheckBox] [int] NULL,
	[LrCharge] [decimal](18, 0) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
 CONSTRAINT [PK_BillMaster] PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientMaster](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](100) NULL,
	[ClientAddress] [varchar](100) NULL,
	[ClientGst] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
 CONSTRAINT [PK_ClientMaster] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LRMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LRMaster](
	[LRId] [int] IDENTITY(1,1) NOT NULL,
	[BillNo] [varchar](50) NULL,
	[LRNumber] [varchar](10) NULL,
	[LRDate] [date] NULL,
	[Package] [int] NULL,
	[PackageType] [int] NULL,
	[PONumber] [varchar](50) NULL,
	[DCNumber] [varchar](50) NULL,
	[STNNumber] [varchar](50) NULL,
 CONSTRAINT [PK_LRMaster] PRIMARY KEY CLUSTERED 
(
	[LRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OwnerMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OwnerMaster](
	[OwnerId] [int] IDENTITY(1,1) NOT NULL,
	[OwnerName] [varchar](100) NULL,
	[OwnerAddress] [varchar](200) NULL,
	[OwnerMobile] [varchar](50) NULL,
	[OwnerEmail] [varchar](50) NULL,
	[OwnerProprietor] [varchar](50) NULL,
	[OwnerGst] [varchar](50) NULL,
	[OwnerPAN] [varchar](50) NULL,
	[OwnerSignature] [varchar](max) NULL,
	[OwnerLogo] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_OwnerMaster] PRIMARY KEY CLUSTERED 
(
	[OwnerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackageTypeMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageTypeMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageTypeName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_PackageTypeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillNo] [varchar](20) NULL,
	[BillDate] [date] NULL,
	[FromStation] [int] NULL,
	[ToStation] [int] NULL,
	[ClientId] [int] NULL,
	[TotalAmt] [decimal](18, 2) NULL,
	[TotalAmtWords] [varchar](100) NULL,
	[BillSubmittedDate] [date] NULL,
	[BillSubmitted] [bit] NULL,
	[CreditPeriod] [int] NULL,
	[Remark] [varchar](200) NULL,
	[PaymentReceived] [bit] NULL,
	[PaymentDate] [date] NULL,
	[PaymentInvoiceNo] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
 CONSTRAINT [PK_PaymentMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StationMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StationMaster](
	[StationId] [int] IDENTITY(1,1) NOT NULL,
	[StationName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_StationMaster] PRIMARY KEY CLUSTERED 
(
	[StationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 6/28/2020 5:07:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserMaster](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[UserEmail] [varchar](50) NULL,
	[Password] [varchar](20) NULL,
	[RoleCode] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoleMaster]    Script Date: 6/28/2020 5:07:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoleMaster](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](20) NULL,
	[RoleCode] [varchar](10) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserRoleMaster] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WeightMaster]    Script Date: 6/28/2020 5:07:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeightMaster](
	[WeightId] [int] IDENTITY(1,1) NOT NULL,
	[WeightName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_WeightMaster] PRIMARY KEY CLUSTERED 
(
	[WeightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BillMaster] ON 

INSERT [dbo].[BillMaster] ([BillId], [BillNo], [BillDate], [ClientId], [FromStation], [ToStation], [WeightId], [Freight], [Loading], [UnLoading], [DetentionCharge], [TotalAmt], [TotalAmtWords], [Remarks], [OwnerId], [CheckBox], [LrCharge], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (15, N'104/2020-2021', CAST(N'2020-04-25' AS Date), 1, 2, 1, 1, CAST(25100.00 AS Decimal(18, 2)), CAST(1450.00 AS Decimal(18, 2)), CAST(2050.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(28600.00 AS Decimal(18, 2)), N'Twenty-Eight thousand Six hundred ', NULL, 1006, 1, CAST(50 AS Decimal(18, 0)), 1, CAST(N'2020-04-25T23:40:41.313' AS DateTime), 0, CAST(N'2020-04-25T23:40:41.330' AS DateTime))
INSERT [dbo].[BillMaster] ([BillId], [BillNo], [BillDate], [ClientId], [FromStation], [ToStation], [WeightId], [Freight], [Loading], [UnLoading], [DetentionCharge], [TotalAmt], [TotalAmtWords], [Remarks], [OwnerId], [CheckBox], [LrCharge], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (16, N'105/2020-2021', CAST(N'2020-04-25' AS Date), 1, 1, 2, 1, CAST(25100.00 AS Decimal(18, 2)), CAST(1450.00 AS Decimal(18, 2)), CAST(2050.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(28600.00 AS Decimal(18, 2)), N'Twenty-Eight thousand Six hundred ', NULL, 1006, 2, CAST(50 AS Decimal(18, 0)), 1, CAST(N'2020-04-25T23:49:15.237' AS DateTime), 0, CAST(N'2020-04-25T23:49:15.237' AS DateTime))
INSERT [dbo].[BillMaster] ([BillId], [BillNo], [BillDate], [ClientId], [FromStation], [ToStation], [WeightId], [Freight], [Loading], [UnLoading], [DetentionCharge], [TotalAmt], [TotalAmtWords], [Remarks], [OwnerId], [CheckBox], [LrCharge], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (20, N'320/2020-2021', CAST(N'2020-05-13' AS Date), 1, 2, 1, 2, CAST(11750.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(13300.00 AS Decimal(18, 2)), N'Thirteen thousand Three hundred ', N'abhishek', 1006, 0, NULL, 1, CAST(N'2020-05-13T21:24:25.927' AS DateTime), 0, CAST(N'2020-05-13T21:24:25.960' AS DateTime))
INSERT [dbo].[BillMaster] ([BillId], [BillNo], [BillDate], [ClientId], [FromStation], [ToStation], [WeightId], [Freight], [Loading], [UnLoading], [DetentionCharge], [TotalAmt], [TotalAmtWords], [Remarks], [OwnerId], [CheckBox], [LrCharge], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (33, N'106/2020-2021', CAST(N'2020-06-02' AS Date), 1, 2, 1, 2, CAST(14100.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), CAST(750.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(15500.00 AS Decimal(18, 2)), N'Fifteen thousand Five hundred ', N'testing2', 1006, 0, CAST(50 AS Decimal(18, 0)), 1, CAST(N'2020-06-05T15:16:27.780' AS DateTime), 0, CAST(N'2020-06-05T15:16:27.780' AS DateTime))
SET IDENTITY_INSERT [dbo].[BillMaster] OFF
SET IDENTITY_INSERT [dbo].[ClientMaster] ON 

INSERT [dbo].[ClientMaster] ([ClientId], [ClientName], [ClientAddress], [ClientGst], [IsActive], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (1, N'Sanofi India Ltd,', N'Mumbai', N'27AA6CC76GST', 1, 1, CAST(N'2020-04-13T21:47:26.790' AS DateTime), 1, CAST(N'2020-04-13T21:47:26.790' AS DateTime))
SET IDENTITY_INSERT [dbo].[ClientMaster] OFF
SET IDENTITY_INSERT [dbo].[LRMaster] ON 

INSERT [dbo].[LRMaster] ([LRId], [BillNo], [LRNumber], [LRDate], [Package], [PackageType], [PONumber], [DCNumber], [STNNumber]) VALUES (1, N'104/2020-2021', N'17A801', CAST(N'2020-04-25' AS Date), 1451, 1, NULL, N'11111111', NULL)
INSERT [dbo].[LRMaster] ([LRId], [BillNo], [LRNumber], [LRDate], [Package], [PackageType], [PONumber], [DCNumber], [STNNumber]) VALUES (2, N'104/2020-2021', N'17A802', CAST(N'2020-04-25' AS Date), 1386, 1, NULL, N'11111222', NULL)
INSERT [dbo].[LRMaster] ([LRId], [BillNo], [LRNumber], [LRDate], [Package], [PackageType], [PONumber], [DCNumber], [STNNumber]) VALUES (3, N'320/2020-2021', N'17A849', CAST(N'2020-05-13' AS Date), 458, 1, N'E002426822', NULL, N'2020017600')
INSERT [dbo].[LRMaster] ([LRId], [BillNo], [LRNumber], [LRDate], [Package], [PackageType], [PONumber], [DCNumber], [STNNumber]) VALUES (11, N'105/2020-2021', N'17A888', CAST(N'2020-05-27' AS Date), 222, 1, N'E002426822', NULL, N'2020017601')
INSERT [dbo].[LRMaster] ([LRId], [BillNo], [LRNumber], [LRDate], [Package], [PackageType], [PONumber], [DCNumber], [STNNumber]) VALUES (15, N'106/2020-2021', N'17A991', CAST(N'2020-06-02' AS Date), 343, 1, N'E002426822', NULL, N'2020017599')
SET IDENTITY_INSERT [dbo].[LRMaster] OFF
SET IDENTITY_INSERT [dbo].[OwnerMaster] ON 

INSERT [dbo].[OwnerMaster] ([OwnerId], [OwnerName], [OwnerAddress], [OwnerMobile], [OwnerEmail], [OwnerProprietor], [OwnerGst], [OwnerPAN], [OwnerSignature], [OwnerLogo], [IsActive], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (1006, N'Shree Raj Tempo Service', N'Ho. Plot No.2507, Opp. Asian Paints(I), Main Road, G.I.D.C, Ankleshwar-393002, Dist.: Bharuch.', N'7600368025', N'abhishek@gmail.com', N'Abhishek Pandey', N'27AAjdnkjandkja6', N'CPFPP543H', N'Abhishek', N'abhee', 1, 0, CAST(N'2020-05-06T19:41:35.320' AS DateTime), 1, CAST(N'2020-05-06T19:41:35.320' AS DateTime))
SET IDENTITY_INSERT [dbo].[OwnerMaster] OFF
SET IDENTITY_INSERT [dbo].[PackageTypeMaster] ON 

INSERT [dbo].[PackageTypeMaster] ([Id], [PackageTypeName], [IsActive]) VALUES (1, N'Cases', 1)
INSERT [dbo].[PackageTypeMaster] ([Id], [PackageTypeName], [IsActive]) VALUES (2, N'Drum', 1)
SET IDENTITY_INSERT [dbo].[PackageTypeMaster] OFF
SET IDENTITY_INSERT [dbo].[PaymentMaster] ON 

INSERT [dbo].[PaymentMaster] ([Id], [BillNo], [BillDate], [FromStation], [ToStation], [ClientId], [TotalAmt], [TotalAmtWords], [BillSubmittedDate], [BillSubmitted], [CreditPeriod], [Remark], [PaymentReceived], [PaymentDate], [PaymentInvoiceNo], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (3, N'105/2020-2021', CAST(N'2020-04-28' AS Date), 1, 2, 1, CAST(28600.00 AS Decimal(18, 2)), N'Twenty-Eight thousand Six hundred ', CAST(N'2020-05-05' AS Date), 1, 0, NULL, 1, CAST(N'2020-06-03' AS Date), N'10013478', 0, CAST(N'2020-06-06T21:49:26.833' AS DateTime), 1, CAST(N'2020-06-06T21:49:26.833' AS DateTime))
SET IDENTITY_INSERT [dbo].[PaymentMaster] OFF
SET IDENTITY_INSERT [dbo].[StationMaster] ON 

INSERT [dbo].[StationMaster] ([StationId], [StationName], [IsActive]) VALUES (1, N'Bhiwandi', 1)
INSERT [dbo].[StationMaster] ([StationId], [StationName], [IsActive]) VALUES (2, N'Ankleshwar', 1)
SET IDENTITY_INSERT [dbo].[StationMaster] OFF
SET IDENTITY_INSERT [dbo].[UserMaster] ON 

INSERT [dbo].[UserMaster] ([UserId], [UserName], [UserEmail], [Password], [RoleCode], [IsActive], [CreatedBy], [CreatedDateTime], [UpdatedBy], [UpdatedDateTime]) VALUES (1, N'Abhishek Pandey', N'srts.ank@yahoo.com', N'srts', 1, 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserMaster] OFF
SET IDENTITY_INSERT [dbo].[UserRoleMaster] ON 

INSERT [dbo].[UserRoleMaster] ([RoleId], [RoleName], [RoleCode], [IsActive]) VALUES (1, N'Admin', N'A', 1)
SET IDENTITY_INSERT [dbo].[UserRoleMaster] OFF
SET IDENTITY_INSERT [dbo].[WeightMaster] ON 

INSERT [dbo].[WeightMaster] ([WeightId], [WeightName], [IsActive]) VALUES (1, N'40Ft Container', 1)
INSERT [dbo].[WeightMaster] ([WeightId], [WeightName], [IsActive]) VALUES (2, N'20Ft Container', 1)
SET IDENTITY_INSERT [dbo].[WeightMaster] OFF
ALTER TABLE [dbo].[BillMaster]  WITH CHECK ADD  CONSTRAINT [FK_BillMaster_ClientMaster] FOREIGN KEY([ClientId])
REFERENCES [dbo].[ClientMaster] ([ClientId])
GO
ALTER TABLE [dbo].[BillMaster] CHECK CONSTRAINT [FK_BillMaster_ClientMaster]
GO
ALTER TABLE [dbo].[BillMaster]  WITH CHECK ADD  CONSTRAINT [FK_BillMaster_OwnerMaster] FOREIGN KEY([OwnerId])
REFERENCES [dbo].[OwnerMaster] ([OwnerId])
GO
ALTER TABLE [dbo].[BillMaster] CHECK CONSTRAINT [FK_BillMaster_OwnerMaster]
GO
ALTER TABLE [dbo].[BillMaster]  WITH CHECK ADD  CONSTRAINT [FK_BillMaster_StationMaster_From] FOREIGN KEY([FromStation])
REFERENCES [dbo].[StationMaster] ([StationId])
GO
ALTER TABLE [dbo].[BillMaster] CHECK CONSTRAINT [FK_BillMaster_StationMaster_From]
GO
ALTER TABLE [dbo].[BillMaster]  WITH CHECK ADD  CONSTRAINT [FK_BillMaster_StationMaster_To] FOREIGN KEY([ToStation])
REFERENCES [dbo].[StationMaster] ([StationId])
GO
ALTER TABLE [dbo].[BillMaster] CHECK CONSTRAINT [FK_BillMaster_StationMaster_To]
GO
ALTER TABLE [dbo].[BillMaster]  WITH CHECK ADD  CONSTRAINT [FK_BillMaster_WeightMaster] FOREIGN KEY([WeightId])
REFERENCES [dbo].[WeightMaster] ([WeightId])
GO
ALTER TABLE [dbo].[BillMaster] CHECK CONSTRAINT [FK_BillMaster_WeightMaster]
GO
ALTER TABLE [dbo].[UserMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_UserRoleMaster] FOREIGN KEY([RoleCode])
REFERENCES [dbo].[UserRoleMaster] ([RoleId])
GO
ALTER TABLE [dbo].[UserMaster] CHECK CONSTRAINT [FK_UserMaster_UserRoleMaster]
GO
